# README

## Requirements

- Rails 5.0
- [httpie](https://httpie.org/)

## Creating API-Only Application

```bash
rails new api-only --api -T -B
cd api-only
```

#### Gemfile

```ruby
group :development do
  # ...
  gem 'faker'
  gem 'hirb'
end
```

```bash
bundle install
```

```bash
rails g model Article title:string body:text
rails db:migrate
```

#### models/article.rb

```ruby
class Article < ApplicationRecord
  validates :title, presence: true
  validates :body,  presence: true
end
```

#### db/seeds.rb

```ruby
5.times do
  Article.create(
    {
      title: Faker::Book.title,
      body:  Faker::Lorem.sentence
    }
  )
end
```

```bash
rails db:seed
```

## have a look from the rails console

```bash
rails c
Hirb.enable
Article.all
```

#### output 

```
  Article Load (0.5ms)  SELECT "articles".* FROM "articles"
+----+-------------------+------------------+-------------------+-------------------+
| id | title             | body             | created_at        | updated_at        |
+----+-------------------+------------------+-------------------+-------------------+
| 1  | An Acceptable ... | Voluptas repe... | 2018-10-30 08:... | 2018-10-30 08:... |
| 2  | Endless Night     | Labore et nis... | 2018-10-30 08:... | 2018-10-30 08:... |
| 3  | Recalled to Life  | Ipsam et dict... | 2018-10-30 08:... | 2018-10-30 08:... |
| 4  | Alone on a Wid... | Voluptatibus ... | 2018-10-30 08:... | 2018-10-30 08:... |
| 5  | A Confederacy ... | Qui beatae es... | 2018-10-30 08:... | 2018-10-30 08:... |
+----+-------------------+------------------+-------------------+-------------------+
```

## create api folder in controllers folder and controllers file

```bash
mkdir -p app/controllers/api/v1
touch app/controllers/api/v1/articles_controller.rb
```

## create api controller

#### app/controllers/api/v1/articles_controller.rb

```ruby
module Api
  module V1
    class ArticlesController < ApplicationController
      def index
        articles = Article.order('created_at DESC')
        render json: {
          status:  'SUCCESS',
          message: 'Loaded articles',
          data:    articles
        },
        status: :ok
      end

      def show
        article = Article.find(params[:id])
        render json: {
          status: 'SUCCESS',
          message: 'Loaded article',
          data: article
        },
        status: :ok
      end

      def create
        article = Article.new(article_params)

        if article.save
          render json: {
            status: 'SUCCESS',
            message: 'Saved article',
            data: article
          },
          status: :ok
        else
          render json: {
            status: 'ERROR',
            message: 'Article not saved',
            data: article.errors
          },
          status: :unprocessable_entity
        end
      end

      def destroy
        article = Article.find(params[:id])
        article.destroy
        
        render json: {
          status: 'SUCCESS',
          message: 'Deleted article',
          data: article
        }
      end

      def update
        article = Article.find(params[:id])
        if article.update_attributes(article_params)
          render json: {
            status: 'SUCCESS',
            message: 'Updated article',
            data: article
          }
        else
          render json: {
            status: 'ERROR',
            message: 'Article not updated',
            data: article.errors
          },
          status: :unprocessable_entity
        end
      end

      private

      def article_params
        params.permit(:title, :body)
      end
    end
  end
end
```

## add routes

#### config/routes.rb

```ruby
Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :articles
    end
  end
end
```

verify routes config

```bash
rails routes
```

## get API data using httpie

start rails server and test it from an another terminal

```bash
# terminal 1
rails s

# terminal 2
http get :3000/api/v1/articles
```

## httpie commands

### Index

```bash
http GET :3000/api/v1/articles
```

#### output

```ruby
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"cdbd7c7107ed4a002094f4e73ed3aee5"
Transfer-Encoding: chunked
X-Request-Id: 17e03ee2-8f31-4996-8b57-57b1d7351089
X-Runtime: 0.005404

{
    "data": [
        {
            "body": "Qui beatae est atque.",
            "created_at": "2018-10-30T08:41:58.533Z",
            "id": 5,
            "title": "A Confederacy of Dunces",
            "updated_at": "2018-10-30T08:41:58.533Z"
        },
        {
            "body": "Voluptatibus corrupti ratione corporis.",
            "created_at": "2018-10-30T08:41:58.530Z",
            "id": 4,
            "title": "Alone on a Wide, Wide Sea",
            "updated_at": "2018-10-30T08:41:58.530Z"
        },
        {
            "body": "Ipsam et dicta dolores.",
            "created_at": "2018-10-30T08:41:58.526Z",
            "id": 3,
            "title": "Recalled to Life",
            "updated_at": "2018-10-30T08:41:58.526Z"
        },
        {
            "body": "Labore et nisi laboriosam.",
            "created_at": "2018-10-30T08:41:58.523Z",
            "id": 2,
            "title": "Endless Night",
            "updated_at": "2018-10-30T08:41:58.523Z"
        },
        {
            "body": "Voluptas repellendus dolorem ut.",
            "created_at": "2018-10-30T08:41:58.517Z",
            "id": 1,
            "title": "An Acceptable Time",
            "updated_at": "2018-10-30T08:41:58.517Z"
        }
    ],
    "message": "Loaded articles",
    "status": "SUCCESS"
}
```

### Show

```bash
http GET :3000/api/v1/articles/1
```

#### output

```ruby
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"6fe1ef841f8595a3f0216beb36d82ccb"
Transfer-Encoding: chunked
X-Request-Id: 401d6fa6-86d9-4fd6-a7dc-dba674be2084
X-Runtime: 0.012922

{
    "data": {
        "body": "Voluptas repellendus dolorem ut.",
        "created_at": "2018-10-30T08:41:58.517Z",
        "id": 1,
        "title": "An Acceptable Time",
        "updated_at": "2018-10-30T08:41:58.517Z"
    },
    "message": "Loaded article",
    "status": "SUCCESS"
}
```

### Create

```bash
http POST :3000/api/v1/articles title=demo_title body=body_demo
```

#### output

```ruby
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"96c827a3620141610b047e2d2a63f730"
Transfer-Encoding: chunked
X-Request-Id: c32decf9-3fcb-464f-9cfc-c2f2defc4a5b
X-Runtime: 0.010139

{
    "data": {
        "body": "body_demo",
        "created_at": "2018-10-30T08:46:03.841Z",
        "id": 6,
        "title": "demo_title",
        "updated_at": "2018-10-30T08:46:03.841Z"
    },
    "message": "Saved article",
    "status": "SUCCESS"
}
```

### Destroy

```bash
http DELETE :3000/api/v1/articles/6
```

#### output

```ruby
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"5e0e20cec233b97e7e0925b1d011fb34"
Transfer-Encoding: chunked
X-Request-Id: 1d3dff81-a10a-4f87-80f0-d56de851f1fb
X-Runtime: 0.008272

{
    "data": {
        "body": "body_demo",
        "created_at": "2018-10-30T08:46:03.841Z",
        "id": 6,
        "title": "demo_title",
        "updated_at": "2018-10-30T08:46:03.841Z"
    },
    "message": "Deleted article",
    "status": "SUCCESS"
}
```

### Update

```bash
http PUT :3000/api/v1/articles/2 title='new updated title' body='new updated body'
```

#### output

```ruby
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"6dbaaa16b3a583ed1f115d21469783ce"
Transfer-Encoding: chunked
X-Request-Id: f99d6b0a-d3f9-4d83-b37b-9a9e55a303f7
X-Runtime: 0.171206

{
    "data": {
        "body": "new updated body",
        "created_at": "2018-10-30T08:41:58.523Z",
        "id": 2,
        "title": "new updated title",
        "updated_at": "2018-10-30T08:49:45.450Z"
    },
    "message": "Updated article",
    "status": "SUCCESS"
}
```
